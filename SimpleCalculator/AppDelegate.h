//
//  AppDelegate.h
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

