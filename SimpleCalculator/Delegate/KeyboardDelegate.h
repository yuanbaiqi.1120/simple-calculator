//
//  KeyboardDelegate.h
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#ifndef KeyboardDelegate_h
#define KeyboardDelegate_h

@protocol KeyboardDelegate
@required
- (void)handleCommand:(NSString *)command;

@end

#endif /* KeyboardDelegate_h */
