//
//  KeyboardView.m
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import "KeyboardView.h"
#import "CommandUtils.h"
#import "CalculatorRoundButton.h"
#import <Masonry/Masonry.h>
@interface KeyboardView ()

@property(nonatomic, copy)NSArray *keys;
@property(nonatomic, weak)CalculatorRoundButton *clickedOperator;
@property(nonatomic, strong)CalculatorRoundButton *clearButton;

- (void)initSubview;
- (CalculatorButtonStyle)buttonStyleByRow:(int)row Col:(int)col;
- (void)buttonClicked:(CalculatorRoundButton *)button;

@end

@implementation KeyboardView

- (instancetype)init {
    self = [super init];
    if(!self) return nil;
    
    self.keys = @[@[@"AC", @"+/-", @"%", @"÷"], //÷
                  @[@"7", @"8", @"9", @"×"], //×
                  @[@"4", @"5", @"6", @"-"],
                  @[@"1", @"2", @"3", @"+"],
                  @[@"0", @"", @".", @"="]];
    
    [self initSubview];
    return self;
}

- (void)initSubview {
    UIView *superView = self;
    NSMutableArray *allButtonArray = [NSMutableArray array];

    for(int row=0; row<5; row++) {
        NSArray *rowArray = [self.keys objectAtIndex:row];
        NSMutableArray *rowButtonArray = [NSMutableArray array];
        for(int col=0; col<4; col++) {
            NSString *keyString = [rowArray objectAtIndex:col];
            CalculatorRoundButton *button = [[CalculatorRoundButton alloc] initWithTitleText:keyString Style:[self buttonStyleByRow:row Col:col]];
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [superView addSubview:button];
            [rowButtonArray addObject:button];
        }
        [rowButtonArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:5 leadSpacing:0 tailSpacing:0];
        [allButtonArray addObject:rowButtonArray];
    }
    
    for(int col=0; col<4; col++) {
        NSMutableArray *colButtonArray = [NSMutableArray array];
        for(int row=0; row<5; row++) {
            CalculatorRoundButton *button = [[allButtonArray objectAtIndex:row] objectAtIndex:col];
            [colButtonArray addObject:button];
        }
        [colButtonArray mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedSpacing:5 leadSpacing:0 tailSpacing:0];
    }
    
    CalculatorRoundButton *fakeZeroButton = [[allButtonArray objectAtIndex:4] objectAtIndex:0];
    CalculatorRoundButton *dummyButton = [[allButtonArray objectAtIndex:4] objectAtIndex:1];
    self.clearButton = [[allButtonArray objectAtIndex:0] objectAtIndex:0];
    
    CalculatorRoundButton *zeroButton = [[CalculatorRoundButton alloc] initWithTitleText:@"0" Style:NumericStyle];
    [zeroButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [superView addSubview:zeroButton];
    [zeroButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fakeZeroButton.mas_top);
        make.bottom.equalTo(fakeZeroButton.mas_bottom);
        make.left.equalTo(fakeZeroButton.mas_left);
        make.right.equalTo(dummyButton.mas_right);
    }];
    
    fakeZeroButton.hidden = YES;
    dummyButton.hidden = YES;
}

- (CalculatorButtonStyle)buttonStyleByRow:(int)row Col:(int)col {
    if(row == 0 && col < 3) {
        return FunctionStyle;
    }
    else if(col == 3) {
        return OperatorStyle;
    }
    return NumericStyle;
}

- (void)buttonClicked:(CalculatorRoundButton *)button {
    NSString *command = button.titleLabel.text;
    if([CommandUtils isOperator:command]) {
        if(self.clickedOperator != nil)
            [self.clickedOperator operatorNormalChangeStyle];
        [button operatorChosenChangeStyle];
        self.clickedOperator = button;
    }
    else if([CommandUtils isNumeric:command]) {
        if(self.clickedOperator != nil) {
            [self.clickedOperator operatorNormalChangeStyle];
            self.clickedOperator = nil;
        }
    }
    if([self.clearButton.titleLabel.text isEqualToString:@"C"] && [command isEqualToString:@"C"]) {
        [self.clearButton setTitle:@"AC" forState:UIControlStateNormal];
    }
    if(button != self.clearButton) {
        [self.clearButton setTitle:@"C" forState:UIControlStateNormal];
    }
    [self.delegate handleCommand:command];
}

@end
