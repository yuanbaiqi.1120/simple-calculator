//
//  RoundButton.m
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import "CalculatorRoundButton.h"
#import <Masonry/Masonry.h>

@implementation CalculatorRoundButton

- (instancetype)initWithTitleText:(NSString *)titleText Style:(CalculatorButtonStyle)style {
    self = [super init];
    if(!self) return nil;
    
    self.layer.cornerRadius = 30;
    [self setTitle:titleText forState:UIControlStateNormal];
    switch(style) {
        case OperatorStyle:
            self.backgroundColor = [UIColor orangeColor];
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:30];
            break;
            
        case NumericStyle:
            self.backgroundColor = [UIColor darkGrayColor];
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:30];
            break;
            
        case FunctionStyle:
            self.backgroundColor = [UIColor lightGrayColor];
            [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:30];
            break;
            
        default:
            break;
    }
    return self;
}

- (void)operatorChosenChangeStyle {
    self.backgroundColor = [UIColor whiteColor];
    [self setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
}

- (void)operatorNormalChangeStyle {
    self.backgroundColor = [UIColor orangeColor];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

@end
