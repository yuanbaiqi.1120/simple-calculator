//
//  KeyboardView.h
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import <UIKit/UIKit.h>
#import "CalculatorRoundButton.h"
#import "KeyboardDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface KeyboardView : UIView

@property(nonatomic, weak, nullable) id<KeyboardDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
