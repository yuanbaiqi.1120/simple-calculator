//
//  RoundButton.h
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, CalculatorButtonStyle) {
    OperatorStyle,
    NumericStyle,
    FunctionStyle
};

@interface CalculatorRoundButton : UIButton

- (instancetype)initWithTitleText:(NSString *)titleText Style:(CalculatorButtonStyle)style;
- (void)operatorChosenChangeStyle;
- (void)operatorNormalChangeStyle;

@end

NS_ASSUME_NONNULL_END
