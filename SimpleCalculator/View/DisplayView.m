//
//  DisplayView.m
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import "DisplayView.h"
#import <Masonry/Masonry.h>

@interface DisplayView ()

@end

@implementation DisplayView

- (instancetype)init {
    self = [super init];
    if(!self) return nil;
    
    self.displayLabel = [[UILabel alloc] init];
    self.displayLabel.text = @"0";
    self.displayLabel.numberOfLines = 0;
    self.displayLabel.font = [UIFont fontWithName:@"Helvetica" size:50];
    self.displayLabel.textColor = [UIColor whiteColor];
    self.displayLabel.textAlignment = NSTextAlignmentRight;
    self.displayLabel.baselineAdjustment = UIBaselineAdjustmentNone;
    [self addSubview:self.displayLabel];
    
    UIView *superView = self;
    [self.displayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
        make.height.mas_equalTo(50);
    }];
    
    return self;
}

@end
