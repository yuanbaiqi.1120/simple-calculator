//
//  DisplayView.h
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DisplayView : UIView

@property(nonatomic, strong)UILabel *displayLabel;

@end

NS_ASSUME_NONNULL_END
