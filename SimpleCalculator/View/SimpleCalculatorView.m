//
//  SimpleCalculatorView.m
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import "SimpleCalculatorView.h"
#import "CalculatorRoundButton.h"
#import "KeyboardView.h"
#import "DisplayView.h"
#import <Masonry/Masonry.h>

@interface SimpleCalculatorView ()

@end

@implementation SimpleCalculatorView

- (instancetype)init {
    self = [super init];
    if(!self) return nil;
    self.backgroundColor = [UIColor blackColor];
    
    self.keyboardView = [[KeyboardView alloc] init];
    [self addSubview:self.keyboardView];
    self.displayView = [[DisplayView alloc] init];
    [self addSubview:self.displayView];
    
    UIView *superView = self;
    
    [self.keyboardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom).offset(-40);
        make.left.equalTo(superView.mas_left).offset(20);
        make.right.equalTo(superView.mas_right).offset(-20);
        make.height.equalTo(superView.mas_height).multipliedBy(0.5);
    }];
    
    [self.displayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView.mas_left).offset(20);
        make.right.equalTo(superView.mas_right).offset(-20);
        make.bottom.equalTo(self.keyboardView.mas_top).offset(-20);
        make.height.equalTo(superView.mas_height).multipliedBy(0.3);
    }];
    
    
    
    return self;
}

@end
