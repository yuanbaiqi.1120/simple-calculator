//
//  SimpleCalculatorView.h
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import <UIKit/UIKit.h>
#import "KeyboardView.h"
#import "DisplayView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SimpleCalculatorView : UIView

@property(nonatomic, strong)KeyboardView *keyboardView;
@property(nonatomic, strong)DisplayView *displayView;

@end

NS_ASSUME_NONNULL_END
