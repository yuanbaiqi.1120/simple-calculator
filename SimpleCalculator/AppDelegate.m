//
//  AppDelegate.m
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import "AppDelegate.h"
#import "SimpleCalculatorViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    SimpleCalculatorViewController *simpleCalculatorViewController = [[SimpleCalculatorViewController alloc] init];
    self.window.rootViewController = simpleCalculatorViewController;
    [self.window makeKeyAndVisible];
    return YES;
}

@end
