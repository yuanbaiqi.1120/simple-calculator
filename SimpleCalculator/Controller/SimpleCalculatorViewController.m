//
//  SimpleCalculatorViewController.m
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/8.
//

#import "SimpleCalculatorViewController.h"
#import "SimpleCalculatorView.h"
#import "KeyboardDelegate.h"
#import "CommandUtils.h"
#import <Masonry/Masonry.h>

@interface SimpleCalculatorViewController () <KeyboardDelegate>

@property(nonatomic, strong) SimpleCalculatorView *simpleCalculatorView;
@property(nonatomic, copy)NSNumber *prevNumber;
@property(nonatomic, copy)NSString *prevOperator;
@property(nonatomic, copy)NSNumber *lastOperateNumber;
@property(nonatomic, copy)NSString *lastOperator;
@property(nonatomic) BOOL isNextNumberStartEditing;

@end

@implementation SimpleCalculatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isNextNumberStartEditing = NO;
    self.simpleCalculatorView = [[SimpleCalculatorView alloc] init];
    self.simpleCalculatorView.keyboardView.delegate = self;
    [self.view addSubview:self.simpleCalculatorView];
    
    UIView *superView = self.view;
    [self.simpleCalculatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.mas_top);
        make.bottom.equalTo(superView.mas_bottom);
        make.left.equalTo(superView.mas_left);
        make.right.equalTo(superView.mas_right);
    }];
}

- (void)handleCommand:(NSString *)command {
    NSString *displayText = self.simpleCalculatorView.displayView.displayLabel.text;
    if([displayText isEqualToString:@"错误"] && ![command isEqualToString:@"C"]) {
        return;
    }
    NSNumber *currentNumber = [NSNumber numberWithDouble:displayText.doubleValue];
    if([CommandUtils isOperator:command]) {
        self.prevNumber = [NSNumber numberWithDouble:displayText.doubleValue];
        self.prevOperator = command;
        self.isNextNumberStartEditing = YES;
    }
    else if([command isEqualToString:@"="]) {
        if(self.prevNumber != nil) {
            NSNumber *result = [CommandUtils calculateWithFirstNumber:self.prevNumber SecondNumber:currentNumber Command:self.prevOperator];
            if(result == nil) {
                self.simpleCalculatorView.displayView.displayLabel.text = @"错误";
                return;
            }
            self.simpleCalculatorView.displayView.displayLabel.text = [CommandUtils resultDisplayString:result];
            self.lastOperateNumber = currentNumber;
            self.lastOperator = self.prevOperator;
            self.prevNumber = nil;
            self.prevOperator = nil;
            self.isNextNumberStartEditing = YES;
        } else if(self.lastOperateNumber != nil) {
            NSNumber *result = [CommandUtils calculateWithFirstNumber: currentNumber SecondNumber:self.lastOperateNumber Command:self.lastOperator];
            self.simpleCalculatorView.displayView.displayLabel.text = [CommandUtils resultDisplayString:result];
        }
    }
    else if([CommandUtils isNumeric:command]) {
        if([command isEqualToString:@"."]) {
            if(![displayText containsString:@"."]) {
                if(displayText.length == limitLength) return;
                self.simpleCalculatorView.displayView.displayLabel.text = [NSString stringWithFormat:@"%@.", displayText];
            }
        } else {
            if([displayText isEqualToString:@"0"]) {
                self.simpleCalculatorView.displayView.displayLabel.text = command;
            } else if(self.isNextNumberStartEditing) {
                self.simpleCalculatorView.displayView.displayLabel.text = command;
                self.isNextNumberStartEditing = NO;
            } else {
                if(displayText.length == limitLength) return;
                self.simpleCalculatorView.displayView.displayLabel.text = [NSString stringWithFormat:@"%@%@", displayText, command];
            }
        }
    }
    else if([command isEqualToString:@"%"]) {
        double percentValue = currentNumber.doubleValue / 100.0;
        self.simpleCalculatorView.displayView.displayLabel.text = [CommandUtils resultDisplayString:[NSNumber numberWithDouble:percentValue]];
    }
    else if([command isEqualToString:@"+/-"]) {
        double oppositeValue = -currentNumber.doubleValue;
        self.simpleCalculatorView.displayView.displayLabel.text = [CommandUtils resultDisplayString:[NSNumber numberWithDouble:oppositeValue]];
    }
    else if([command isEqualToString:@"C"]) {
        self.simpleCalculatorView.displayView.displayLabel.text = @"0";
        self.lastOperator = nil;
        self.lastOperateNumber = nil;
        self.prevNumber = nil;
        self.prevOperator = nil;
        self.isNextNumberStartEditing = NO;
    }
}

@end
