//
//  CommandUtils.m
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/9.
//

#import "CommandUtils.h"

@implementation CommandUtils

+ (BOOL)isOperator:(NSString *)command {
    return [command isEqualToString:@"+"] || [command isEqualToString:@"-"] || [command isEqualToString:@"×"] || [command isEqualToString:@"÷"];
}

+ (BOOL)isNumeric:(NSString *)command {
    unichar ch = [command characterAtIndex:0];
    return (ch >= '0' && ch <= '9') || ch == '.';
}

+ (NSNumber *)calculateWithFirstNumber:(NSNumber *)firstNumber SecondNumber:(NSNumber *)secondNumber Command:(NSString *)command {
    double doubleFirstNumber = firstNumber.doubleValue;
    double doubleSecondNumber = secondNumber.doubleValue;
    double result = 0.0;
    if([command isEqualToString:@"+"]) {
        result = doubleFirstNumber + doubleSecondNumber;
    } else if([command isEqualToString:@"-"]) {
        result = doubleFirstNumber - doubleSecondNumber;
    } else if([command isEqualToString:@"×"]) {
        result = doubleFirstNumber * doubleSecondNumber;
    } else if([command isEqualToString:@"÷"]) {
        if (fabs(doubleSecondNumber) < zeroError) {
            return nil;
        }
        result = doubleFirstNumber / doubleSecondNumber;
    }
    return [NSNumber numberWithDouble:result];
}

+ (NSString *)resultDisplayString:(NSNumber *)result {
    NSString *resultString = [NSString stringWithFormat:@"%.8lf", result.doubleValue];
    NSInteger index = resultString.length - 1;
    while([resultString characterAtIndex:index] == '0')
        index--;
    if([resultString characterAtIndex:index] == '.')
        index--;
    resultString = [resultString substringWithRange:NSMakeRange(0, index + 1)];
    if(resultString.length > limitLength)
        resultString = [resultString substringWithRange:NSMakeRange(0, limitLength)];
    return resultString;
}

@end
