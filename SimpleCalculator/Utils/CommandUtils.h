//
//  CommandUtils.h
//  SimpleCalculator
//
//  Created by ByteDance on 2022/3/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
const static NSInteger limitLength = 12;
const static double zeroError = 1e-21;

@interface CommandUtils : NSObject

+ (BOOL)isOperator:(NSString *)command;
+ (BOOL)isNumeric:(NSString *)command;

+ (NSNumber *)calculateWithFirstNumber:(NSNumber *)firstNumber SecondNumber:(NSNumber *)secondNumber Command:(NSString *)command;
+ (NSString *)resultDisplayString:(NSNumber *)result;

@end

NS_ASSUME_NONNULL_END
